<!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="modules/css/areas/styles_areas.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">
	<title>R&C Window Cleaning</title>
</head>

<body>
		
	<header>

		<?php include("include/header.php");?>

				
	</header>

	<section id="content">
		
		<section id="main">

			<section id="areas">
				<div>
					<hgroup>
						<h2>Areas where we work</h2>
					
					<p>					
					We cover the following areas in London, Bedfordshire and Milton Keynes:	</p>
					</hgroup>
				
				</div>

				<div id="zona">

					<p class="area">W: West</p>
					<p class="area">WC: West City</p>
					<p class="area">EC: East City</p>
					<p class="area">E: East</p>
					<p class="area">N: North</p>
					<p class="area">NW: NorthWest</p>
					<p class="area">SE: SouthEast</p>
					<p class="area">SW: SouthWest</p>
					<p class="area">LU: Luton, Dunstable</p>
					<p class="area">MK: Milton Keynes</p> 


				</div>
				
				<div>
					<img class="thumb1" src="imagenes/	map-london-postcodes.jpg"/>
				</div>	

				<div id="question">
					<p>Any questions? Give us a call, or fire us an email!</p>

					<p>	info@rcwindowcleaning.co.uk

	
					</p>
				</div>

			</section><!--END Areas-->

			
		</section><!--End main-->


		<div class="sidebar">

			 <?php include("include/sidebar.php");?>
					

		</div>

	</section><!--End content-->
	
		<?php include("include/quote.php");?>


		<footer>
			<?php include("include/footer.php");?>

		</footer>


	</body>
</html>