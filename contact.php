<!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/styles_formcontact.css">

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">
	<title>R&C Window Cleaning</title>
</head>

<body>


<header>

		<?php include("include/header.php");?>

				
</header>

		<section id="content">
				
			<section id="main">

				<div>
					<p class="page_contact">Contact Us</p>
					<p class="page_contact"> Phone: 07482723394</p>
					<p class="page_contact">Email: info@rcwcleanig.co.uk </p>
					<br>
					<p class="page_contact">OR </p>
				</div>

				<?php include("include/contact_form2.php");?>

			</section><!-- End main -->

			<footer>
			<?php include("include/footer.php");?>

			</footer>

		</section><!-- End content -->
	</body>
</html>