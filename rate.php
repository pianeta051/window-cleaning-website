<hgroup>
		<h2 class="h2_price">Prices and Rates</h2>
			
			<p>We charge for levels and types of windows.</p>
			<p>
				Depending on the type and number of windows we can give you a correct price for each level of your property.
			</p>
						<h3 class="h3_price">Windows Types</h3>
</hgroup>
						
						<div class =""parrafo_price">
							<p>
							There are many different types of windows. In most cases we clean the inner and outer part of the windows by gaining access from inside. Each type opens differently and experience is needed to do a good job in cleaning.

							Special window cleaning equipment If necessary we use ladders, extension poles, boom lifts, or descent from the roof using special high rise equipment.


							Besides different frame types there are also different glass types like tinted, coated or tempered glass, which require special skills to clean. This glass can be easily damaged if cleaned by nonprofessionals.
							</p>
						</div>

					<hgroup>

						<h2 class="h2_price">Our Rates</h2>
						<h3 class="h3_price">Screens Cleaning</h3>
					</hgroup>

						<p>
						Big Apple Window cleaning always recommends washing the screens during your window cleaning appointment. This service can be performed in two ways:

						A. We remove screens from their tracks, wash them inside of apartment (we protect floors with towels) or on your backyard where we wash them with a hose and brushes. After your screens are dry – we install them back in their tracks.

						B. We don't remove your screens from their tracks – wet the screen with a washer while it is on the window and then wipe it with special dry cloth which absorbs he dirt. This method is efficient and cost effective.
						</p>

					<hgroup>

						<h3 class="h3_price">Windows Sliding Up and Down</h3>
					</hgroup>
						
						<p>
						These windows slide up and down. They are usually wooden and have hooks outside. We have a special belt that we use to clean the outside of these windows. These windows do not open to the inside of the apartment or office. From inside these windows often look like the sliding windows in the picture above. Picture on the right shows one window.
						</p>

					<hgroup>

						<h3 class="h3_price">Tilt-in Windows (Sliding)</h3>
					</hgroup>
					
						<p>
						These windows slide up and down as well but they tilt-in. Usually they have metal or plastic frames. Both the top and bottom has two clips, one on each side. When these clips are moved to the inside the window can be tilted in for washing. Picture on the right shows one window sliding up and down.
						</p>

					<hgroup>
						<h3 class="h3_price">Storm windows</h3>
					</hgroup>
						
						<p>
						Storm windows are windows installed outside of normal window to protect your home from low temperatures and street noise. Usually the outside window is a thin framed glass that slides up and down. It has to be removed into the apartment in order to be washed. These windows would look like windows sliding up and down on a picture above. Picture on the right shows the process of removing storm window.
						</p>

					<hgroup>
						
						<h3 class="h3_price">Windows and window sets</h3>
					</hgroup>

						<p>
						Picture on the right shows 3 windows, two side ones and one picture window. Some people count this as one window but that's incorrect. It's a window set consisting of 3 windows.

						The two most common types of big panel windows are horizontal sliding windows and door-type windows.

						The sliding windows slide past each other in a horizontal track. Usually one or 2 panels has to be removed inside of the apartment to clean and then installed back. Picture on the right shows 3 windows.


						The door-type windows open to the inside of the apartment just like doors. Often there are three windows in a row, the one in the middle is fixed while the two side ones open like doors.
						</p>

					<hgroup>
						<h3 class="h3_price">City quiet (noise protection) windows</h3>	
						
					</hgroup>	
						
						<p>
						These windows are designed to block the noise from the street. They are usually installed from the inside of the apartment. This means that there is a second set of windows in front of the original windows. They have to be removed into the apartment to be cleaned. The outer window can then be accessed for cleaning as well.
						The picture on the right shows the inner window layer (3 windows) in front of main 3 windows. 

						</p>

					<hgroup>
						<h3 class="h3_price">Multiple pane windows</h3>
					</hgroup>

						<p>
						Small pane windows are French style or similar. They are made of small panes and take much longer to clean. Therefore, every twelve panes counts as one window. 
						Picture on the right would count as 3 windows..
						</p>