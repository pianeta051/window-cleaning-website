 <!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="../../modules/css/styles_service.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">

	<title>R&C Window Cleaning</title>
</head>

<body>


<header>

		<?php include("../../include/header.php");?>

				
</header>

		<section id="content">
				
			<section id="main">
				<div id="roof_phar">
					<hgroup>
						<h2 class ="title">Conservatories and Glass Roofs</h2>
							<img class="thumb" src="../../imagenes/slideshow/glassroof.png"/>
											
								<p id="pharrafo">
									Cleaning of window, roofs, facias and detailed areas of conservatories is important to keep its stunning appearance and more importantly keep it in top condition.
									The lack of a regular maintenance program allows minerals and chemicals to etch or build up on the glass surface and in some cases cause permanent damage. This condition can be caused by a number of reasons including but not limited to, sprinkler systems over spray, water run off from building design, condensation and improper chemical application from prior attempts to remove the stains.The cost of removal can be substantial, although it is much less than the cost of replacement. Many times it is not possible to discover the full extent of damage or determine the price of removal until a Standard Cleaning is performed and viewed by the customer and RC Window Cleaning employee.
								</p>
							<img class="thumb1" src="../../imagenes/slideshow/conservatory.jpg"/>
					</hgroup>
				</div>

		
			</section><!-- End main -->

			<footer>
			<?php include("../../include/footer.php");?>

			</footer>

		</section><!-- End content -->
	</body>
</html>