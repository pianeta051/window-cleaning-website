<!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="../../modules/css/styles_service.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">

	<title>R&C Window Cleaning</title>
</head>

<body>


<header>

		<?php include("../../include/header.php");?>

				
</header>

		<section id="content">
				
			<section id="main">
				<div>
					<hgroup>
						<h2 class ="title">Power Washing</h2>
							<img class="thumb" src="../../imagenes/slideshow/pressionwash.png"/>
								<p id="pharrafo">

									For some instances, power washing is the ideal approach to use. We often use power washing for cleaning a variety of exterior surfaces, with exceptional results. We rely on power washing for essential tasks like cleaning pavers, bricks, vinyl, concrete, stucco and even fencing.

									Whether you are preparing a surface for staining, painting or some other finishing process, or just want to enhance the look, we can do it for you. You want to be able to safeguard your home and business from contaminants that can permanently damage them, or even destroy them completely. This is where we come into the picture.
								</p>
					</hgroup>
							<img class="thumb1" src="../../imagenes/powerwash.jpg"/>
				</div>	

		
			</section><!-- End main -->

			<footer>
			<?php include("../../include/footer.php");?>

			</footer>

		</section><!-- End content -->
	</body>
</html>