 <!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="../../modules/css/styles_service.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">

	<title>R&C Window Cleaning</title>
</head>

<body>


<header>

		<?php include("../../include/header.php");?>

				
</header>

		<section id="content">
				
			<section id="main">
				<div>
					<hgroup>
						<h2 class ="title">Why clean the gutters</h2>
							<img class="thumb" src="../../imagenes/slideshow/gutters.png"/>
								<p id="pharrafo">
													

									Gutters are an integral piece of your home’s well being. They serve to control the flow of rainwater to protect your roof, walls, foundation and landscape. Water is very important to us but it can be very dangerous and expensive if gutters don’t exist to control where the water goes.

									Why should you clean your gutters?

									Leaves, debris, sand from your roof and all kinds of debris that the air may bring love gutters.  They start accumulating over time and without noticing, they will clog your gutters creating an overweight mix of debris and water inside your gutters. This can damage the angle of the gutter causing that little water that may go through all of that debris to stay inside your gutters and overflow causing damages to your basement, roof, siding and landscape.
								</p>
											
					</hgroup>
										
							<img class="thumb1" src="../../imagenes/slideshow/gutters3.jpg"/>
				</div>
	

		
			</section><!-- End main -->

			<footer>
			<?php include("../../include/footer.php");?>

			</footer>

		</section><!-- End content -->
	</body>
</html>