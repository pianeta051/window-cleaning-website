 <!DOCTYPE html>

<html lang="en">

<link rel="stylesheet" type="text/css" href="../../modules/css/styles_service.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">

	<title>R&C Window Cleaning</title>
</head>

<body>


<header>

		<?php include("../../include/header.php");?>

				
</header>

		<section id="content">
				
			<section id="main">
				<div id="window_phar">
					<hgroup>
						<h2 class ="title">Residential  Window Cleaning</h2>
							<img class="thumb" src="../../imagenes/slideshow/windows1.png"/>
									<p id="pharrafo">

										Whether it's spring cleaning, getting ready for the holidays, or just regular maintenance to protect your investment, our residential team will handle your home with care and give it a shine that will make you the pride of the neighborhood. Our approach to residential is very simple: Safely deliver the highest quality service in the least disruptive manner to the household as possible.

										Having your windows professionally cleaned at regular intervals is also an important part of protecting your investment. Neglecting your windows for extended periods of time allows environmental pollutants as well as chemicals that might be leaching from the building itself to build up on the glass. These contaminants, if allowed to remain on the windows, will eventually chemically bond with the glass causing extensive damage that is both labor-intensive (see glass restoration) and expensive to repair.
									 </p>
							 <img class="thumb1" src="../../imagenes/images1.jpg"/>
					</hgroup>
									
				</div>
		
			</section><!-- End main -->

			<footer>
			<?php include("../../include/footer.php");?>

			</footer>

		</section><!-- End content -->
	</body>
</html>