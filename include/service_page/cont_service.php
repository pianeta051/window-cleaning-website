
<div id="window_phar">
	<hgroup>
		<h2 class ="title">Residential  Window Cleaning</h2>
			<img class="thumb" src="imagenes/slideshow/windows1.png"/>
					<p id="pharrafo">

						Whether it's spring cleaning, getting ready for the holidays, or just regular maintenance to protect your investment, our residential team will handle your home with care and give it a shine that will make you the pride of the neighborhood. Our approach to residential is very simple: Safely deliver the highest quality service in the least disruptive manner to the household as possible.

						Having your windows professionally cleaned at regular intervals is also an important part of protecting your investment. Neglecting your windows for extended periods of time allows environmental pollutants as well as chemicals that might be leaching from the building itself to build up on the glass. These contaminants, if allowed to remain on the windows, will eventually chemically bond with the glass causing extensive damage that is both labor-intensive (see glass restoration) and expensive to repair.
					 </p>
			 <img class="thumb1" src="imagenes/images1.jpg"/>
	</hgroup>
					
</div>
					
<div id="roof_phar">
	<hgroup>
		<h2 class ="title">Conservatories and Glass Roofs</h2>
			<img class="thumb" src="imagenes/slideshow/glassroof.png"/>
							
				<p id="pharrafo">
					Cleaning of window, roofs, facias and detailed areas of conservatories is important to keep its stunning appearance and more importantly keep it in top condition.
					The lack of a regular maintenance program allows minerals and chemicals to etch or build up on the glass surface and in some cases cause permanent damage. This condition can be caused by a number of reasons including but not limited to, sprinkler systems over spray, water run off from building design, condensation and improper chemical application from prior attempts to remove the stains.The cost of removal can be substantial, although it is much less than the cost of replacement. Many times it is not possible to discover the full extent of damage or determine the price of removal until a Standard Cleaning is performed and viewed by the customer and RC Window Cleaning employee.
				</p>
			<img class="thumb1" src="imagenes/slideshow/conservatory.jpg"/>
	</hgroup>
</div>


<div>
	<hgroup>
		<h2 class ="title">Why clean the gutters</h2>
			<img class="thumb" src="imagenes/slideshow/gutters.png"/>
				<p id="pharrafo">
									

					Gutters are an integral piece of your home’s well being. They serve to control the flow of rainwater to protect your roof, walls, foundation and landscape. Water is very important to us but it can be very dangerous and expensive if gutters don’t exist to control where the water goes.

					Why should you clean your gutters?

					Leaves, debris, sand from your roof and all kinds of debris that the air may bring love gutters.  They start accumulating over time and without noticing, they will clog your gutters creating an overweight mix of debris and water inside your gutters. This can damage the angle of the gutter causing that little water that may go through all of that debris to stay inside your gutters and overflow causing damages to your basement, roof, siding and landscape.
				</p>
							
	</hgroup>
						
			<img class="thumb1" src="imagenes/slideshow/gutters3.jpg"/>
</div>

<div>
	<hgroup>
		<h2 class ="title">Power Washing</h2>
			<img class="thumb" src="imagenes/slideshow/pressionwash.png"/>
				<p id="pharrafo">

					For some instances, power washing is the ideal approach to use. We often use power washing for cleaning a variety of exterior surfaces, with exceptional results. We rely on power washing for essential tasks like cleaning pavers, bricks, vinyl, concrete, stucco and even fencing.

					Whether you are preparing a surface for staining, painting or some other finishing process, or just want to enhance the look, we can do it for you. You want to be able to safeguard your home and business from contaminants that can permanently damage them, or even destroy them completely. This is where we come into the picture.
				</p>
	</hgroup>
			<img class="thumb1" src="imagenes/powerwash.jpg"/>
</div>	
					
<div>
	<hgroup>
		<h2 class ="title">Sky Lights and Velux Windows</h2>
			<img class="thumb" src="imagenes/slideshow/skylight1.png"/>
				<p id="pharrafo">	
					
						Dirty skylights can really detract from your home's overall appearance. Cleaning them requires the cleaning of the glass surfaces from both the inside and the outside of the estructure. Accessing the skylights for cleaning should only be done by a trained professional. 
						Reaching skylights' glass requires professional equipment, skills and experience. We strongly recommend not to attempt to clean your sun room nor skylights by yourself!. When we clean the skylights for the first time, both surfaces will be cleaned according to normal initial window cleaning or post construction procedure.</p>
	</hgroup>
						
		<img class="thumb1" src="imagenes/skyligh.jpg"/>
</div>