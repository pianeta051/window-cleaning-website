<div id="parrafo0">

	<hgroup>
		<h2 class="h2_price">Prices and Rates</h2>
				
			<p>We charge for levels and types of windows.</p>
			<p>
				Depending on the type and number of windows we can give you a correct price for each level of your property.
			</p>
			<p>
				If you do not know what kind of window you have, here are some examples.
			</p>
				

	</hgroup>
</div>

<div id="parrafo1">
	
	<div>	
		<hgroup>
			<h2>Double-hung sash window</h2>


			<p>This sash window is the traditional style of window in the UK with two parts (sashes) that overlap slightly and slide up and down inside the frame. The two parts are not necessarily the same size. Nowadays, most new double-hung sash windows use spring balances to support the sashes, but traditionally, counterweights held in boxes on either side of the window were used.
			</p>
		</hgroup>
	</div> 
	
	<div>
		<img class="thumb1" src="imagenes/type of windows/sash-window.jpg"/>

	</div>

	
</div>						


<div id="parrafo2">
	
	<div>
		<hgroup>
			<h2>Casement window</h2>


			<p>A window with a hinged sash that swings in or out like a door comprising either a side-hung, top The casement window is the main type of window now found in the UK.
			</p>
		</hgroup>
	</div>
	<div>
		<img class="thumb1" src="imagenes/type of windows/casement-window.jpg"/>
	</div>
</div>

<div id="parrafo3">
	
	<div>	
		<hgroup>
			<h2>Skylight</h2>


			<p>A flat or slope window built into a roof structure that is out of reach.
			</p>
		</hgroup>
	</div>	

	<div>
		<img class="thumb1" src="imagenes/type of windows/skylight.jpg"/>
	</div>

</div>	

<div id="parrafo4">
	
	<div>	
		<hgroup>
			<h2>Roof lantern</h2>


			<p>A roof lantern is a multi-paned glass structure, resembling a small building, built on a roof for day or moon light.
			</p>
		</hgroup>
	</div>

	<div>
		<img class="thumb1" src="imagenes/type of windows/roof-lantern.jpg"/>
	</div>
</div>					

<div id="parrafo5">
	
	<div>
		<hgroup>
			<h2>Bay window</h2>


			<p>A multi-panel window, with at least three panels set at different angles to create a protrusion from the wall.
			</p>
		</hgroup>
	</div>
		
	<div>
		<img class="thumb1" src="imagenes/type of windows/bay-window2.jpg"/>
	</div>
</div>

<div id="parrafo6">
	
	<div>
		<hgroup>
			<h2>Oriel window</h2>


			<p>A window with many panels. It is most often seen in Tudor-style houses and monasteries. An oriel window projects from the wall and does not extend to the ground. Oriel windows started as a form of porch.
			</p>
		</hgroup>
	</div>

	<div>
		<img class="thumb1" src="imagenes/type of windows/oriel-window.jpg"/>
	</div>

</div>

<div id="parrafo7">
	<div>
		<hgroup>
			<h2>Picture window</h2>

			<p>A very large fixed window in a wall, typically without glazing bars, or glazed with only perfunctory glazing bars near the edge of the window. Picture windows are intended to provide an unimpeded view, as if framing a picture.
			</p>
		</hgroup>
	</div>

	<div>
		<img class="thumb1" src="imagenes/type of windows/picture-window.jpg"/>
	</div>
</div>

<div id="parrafo8">
	<div>
		<hgroup>
			<h2>French window</h2>

			<p>A French window, also known as a French door is really a type of door but one which has one or more panes of glass set into the whole length of the door, meaning it also functions as a window. They are very common, usually overlooking a garden or patio.
			</p>
		</hgroup>
	</div>

	<div>
		<img class="thumb1" src="imagenes/type of windows/french-window-door.jpg"/>
	</div>
</div>

<div id="parrafo9">
	<div>
		<hgroup>
			<h2>Leaded Lights & Stained Glass</h2>

			<p>Leaded Lights or lead lights are made up from small panes of glass joined together with lead H section. This is known as came. They individual pieces of lead are then soldered together. Sometimes, especially on larger lead lights, short lengths of copper wire are soldered into The whole panel is then cemented together with a type of putty being pushed into the joints with a scrubbing brush. This is then covered with a white powder (hardener) that removes the moisture contained within the cement. It is then left to cure or set and neatly knifed off. The leaded light is finally cleaned up, polished and blacked up with Zebo™ grate polish.
						
			</p>
		</hgroup>
	</div>

	<div>
		<img class="thumb1" src="imagenes/type of windows/leaded-windows.jpg"/>
	</div>
</div>



							