<!DOCTYPE html>

<html lang="en">


<link rel="stylesheet" type="text/css" href="modules/css/styles_service.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">



<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">
	<title>R&C Window Cleaning</title>
</head>

	<body>
		
		<header>
			<?php include("include/header.php");?>
		</header>

		<section id="content">
			<section id="main">

				<section id="cont_service">

					<?php include("include/service_page/cont_service.php");?>
				</section><!--section cont_service	-->

				

			</section><!--End main-->


			<!--Sidebar (contact form + satisfation)-->
			<?php include("include/sidebar.php");?>
			<!--END Sidebar (contact form + satisfation)-->

		</section><!--End content-->

		<?php include("include/quote.php");?>

		<footer>
			<?php include("include/footer.php");?>

		</footer>






	</body>
</html>
