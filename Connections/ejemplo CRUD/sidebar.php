<section id="satisfaction">
	<div id="">
		<p id="parrafo_satis">
			We offer 100% customer satisfaction of service if for any reason you are not completely satisfied and happy with our work we will come back and clean your windows again. For free.
		</p>
								
	</div>
	
	<div>
		<img src="imagenes/satisfaction.png">
	</div>

</section><!-- End satisfaction-->

<section id="contact">
    <script type="text/javascript">
	    var RecaptchaOptions = {
									theme : "red"
								 };
	</script>
	 
	<hgroup>
		<h1>Request a Call Back</h1>
	</hgroup>
	<form action="<?php echo $req_cliente->id > 0 ? : 'registrar'; ?>" method="post" class="form-horizontal" >

	   <input type="hidden" name="id" value="<?php echo $req_cliente->__GET('id'); ?>" /> 

		    <fieldset>
				<!-- Alert Box-->
						
										
								<!-- Name Field -->
				<div class="control-group">
					<label for="inputName">Name</label>
					<div class="controls">
						<input class="form-control" name="name" type="text" value="<?php echo $req_cliente->__GET('name'); ?>"   
						id="id" placeholder="Name" required>
							<p class="help-block"></p>
					</div>
				</div>
								 
								 <!-- E-mail Field -->
								  			  
				<div class="control-group">
					<label for="inputEmail">Email</label>
					<div class="controls">
						<input class="form-control" name="email" type="email" value="<?php echo $req_cliente->__GET('email'); ?>"  id="id" placeholder="Email" required>
							<p class="help-block"></p>
					</div>
				</div>
								  		  
								  <!-- Phone Field -->
				<div class="control-group">
					<label for="inputPhone">Phone</label>
					<div class="controls">
						<input class="form-control" name="phone" type="number" value="<?php echo $req_cliente->__GET('phone'); ?>" id="id" placeholder="Phone"  >
							<p class="help-block"></p>
					</div>
				</div>
								  		 
								 <!-- Message Field -->
				<div class="control-group">
					<label for="inputMessage">Message</label>
					<div class="controls">
						<textarea class="form-control" name="message" rows="12" value="<?php echo $req_cliente->__GET('message'); ?>" id="id" placeholder="Please include as much detail as possible." minlength="20" required>
							
						</textarea>
						<p class="help-block"></p>
					</div>
				</div>
								  		 
								 <!-- Submit Button -->
				<div class="control-group">
					<div class="controls">
						<button type="submit" name="sbutton" value="Send" class="btn btn-primary">Submit</button>
					</div>
				</div>
			
			</fieldset>
			<input type="hidden" name="MM_insert" value="form_key" />
	</form>

</section>

<!--<?php //include("Connections/capturar_small_form.php");?> -->


<?php
require_once 'request_cliente_small.php';
require_once 'logical_request_small.php';

// Logic
$req_cliente = new Request_cliente();
$model = new Request_cliente_model();

if(isset($_REQUEST['action']))
{
    switch($_REQUEST['action'])
    {
        case 'actualizar':
            $req_cliente->__SET('id',              $_REQUEST['id']);
            $req_cliente->__SET('name',          $_REQUEST['name']);
            $req_cliente->__SET('email',        $_REQUEST['email']);
            $req_cliente->__SET('phone',            $_REQUEST['phone']);
            $req_cliente->__SET('message', $_REQUEST['message']);

            $model->Actualizar($req_cliente);
            header('Location: index.php');
            break;

        case 'registrar':
            $req_cliente->__SET('name',         $_REQUEST['name']);
            $req_cliente->__SET('email',        $_REQUEST['email']);
            $req_cliente->__SET('phone',        $_REQUEST['phone']);
            $req_cliente->__SET('message',      $_REQUEST['message']);

            $model->Registrar($req_cliente);
            header('Location: index.php');
            break;

        case 'eliminar':
            $model->Eliminar($_REQUEST['id']);
            header('Location: index.php');
            break;

        case 'editar':
            $req_cliente = $model->Obtener($_REQUEST['id']);
            break;
    }
}

?>
