
<?php 

class Request_cliente_model
{
    private $pdo;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = new PDO('mysql:host=localhost;dbname=wcleaning', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);                
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM request_call");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r)
            {
                $req_cliente = new Request_cliente();

                $req_cliente->__SET('id', $r->id);
                $req_cliente->__SET('name', $r->name);
                $req_cliente->__SET('email', $r->email);
                $req_cliente->__SET('phone', $r->phone);
                $req_cliente->__SET('message', $r->message);

                $result[] = $req_cliente;
            }

            return $result;
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try 
        {
            $stm = $this->pdo
                      ->prepare("SELECT * FROM request_call WHERE id = ?");
                      

            $stm->execute(array($id));
            $r = $stm->fetch(PDO::FETCH_OBJ);

            $req_cliente = new Request_cliente();

            $req_cliente->__SET('id', $r->id);
            $req_cliente->__SET('name', $r->name);
            $req_cliente->__SET('email', $r->email);
            $req_cliente->__SET('phone', $r->phone);
            $req_cliente->__SET('message', $r->message);

            return $req_cliente;
        } catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try 
        {
            $stm = $this->pdo
                      ->prepare("DELETE FROM request_call WHERE id = ?");                      

            $stm->execute(array($id));
        } catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }

    public function Actualizar(request_cliente $data)
    {
        try 
        {
            $sql = "UPDATE request_call SET 
                        name          = ?, 
                        email        = ?,
                        phone            = ?, 
                        message = ?
                    WHERE id = ?";

            $this->pdo->prepare($sql)
                 ->execute(
                array(
                    $data->__GET('name'), 
                    $data->__GET('email'), 
                    $data->__GET('phone'),
                    $data->__GET('message'),
                    $data->__GET('id')
                    )
                );
        } catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }

    public function Registrar(request_cliente $data)
    {
        try 
        {
        $sql = "INSERT INTO request_call (name,email,phone,message) 
                VALUES (?, ?, ?, ?)";

        $this->pdo->prepare($sql)
             ->execute(
            array(
                $data->__GET('name'), 
                $data->__GET('email'), 
                $data->__GET('phone'),
                $data->__GET('message')
                )
            );
        } catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }
}

?>