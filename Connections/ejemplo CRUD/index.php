<!DOCTYPE html>

<html lang="en">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="js/jquery.slides.min.js"></script>

<script>
  $(function(){
     $("#slideshow").slidesjs({
       play: {
          active: true,
           // [boolean] Generate the play and stop buttons.
           // You cannot use your own buttons. Sorry.
          effect: "slide",
           // [string] Can be either "slide" or "fade".
          interval: 4000,
           // [number] Time spent on each slide in milliseconds.
          auto: true,
           // [boolean] Start playing the slideshow on load.
          swap: true,
           // [boolean] show/hide stop and play buttons
         pauseOnHover: false,
           // [boolean] pause a playing slideshow on hover
          restartDelay: 2500
           // [number] restart delay on inactive slideshow
    }
     });
  });
 </script>

<head>
	<meta charset="utf-8">
	<meta name="description" content="Windows cleaner London">
	<meta name="keywords" content="window cleaning, conservatory cleaning, gutter cleaning, pressure washing">
	<title>R&C Window Cleaning</title>
</head>

	<body>
		
		<header>

			<?php include("../../include/header.php");?>

		</header>

		<section id="content">
			<section id="main">
				<?php include("../../include/slider.php");?>

				<?php include("../../include/bar_service.php");?>
				

			</section><!--End main-->

			<div class="sidebar">

				 <?php include("sidebar.php");?>
					

			</div>
				
		<footer>
			<?php include("../../include/footer.php");?>

		</footer>
		</section><!--End content-->

		<?php include("../../include/quote.php");?>

		
	</body>
</html>