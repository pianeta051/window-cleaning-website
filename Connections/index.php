<?php
require_once 'request_cliente_small.php';
require_once 'logical_request_small.php';

// Logic
$req_cliente = new Request_cliente();
$model = new Request_cliente_model();

if(isset($_REQUEST['action']))
{
    switch($_REQUEST['action'])
    {
        case 'actualizar':
            $req_cliente->__SET('id',              $_REQUEST['id']);
            $req_cliente->__SET('name',          $_REQUEST['name']);
            $req_cliente->__SET('email',        $_REQUEST['email']);
            $req_cliente->__SET('phone',            $_REQUEST['phone']);
            $req_cliente->__SET('message', $_REQUEST['message']);

            $model->Actualizar($req_cliente);
            header('Location: index.php');
            break;

        case 'registrar':
            $req_cliente->__SET('name',         $_REQUEST['name']);
            $req_cliente->__SET('email',        $_REQUEST['email']);
            $req_cliente->__SET('phone',        $_REQUEST['phone']);
            $req_cliente->__SET('message',      $_REQUEST['message']);

            $model->Registrar($req_cliente);
            header('Location: index.php');
            break;

        case 'eliminar':
            $model->Eliminar($_REQUEST['id']);
            header('Location: index.php');
            break;

        case 'editar':
            $req_cliente = $model->Obtener($_REQUEST['id']);
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Anexsoft</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
    </head>
    <body >



        <div class="pure-g">
            <div class="pure-u-1-12">
                
                <form action="?action=<?php echo $req_cliente->id > 0 ? 'actualizar' : 'registrar'; ?>" method="post" class="pure-form pure-form-stacked" >
                    <input type="hidden" name="id" value="<?php echo $req_cliente->__GET('id'); ?>" />
                    
                    <table >
                        <tr>
                            <th >Nombre</th>
                            <td><input type="text" name="name" value="<?php echo $req_cliente->__GET('name'); ?>"  /></td>
                        </tr>
                        <tr>
                            <th >Email</th>
                            <td><input type="text" name="email" value="<?php echo $req_cliente->__GET('email'); ?>"  /></td>
                        </tr>
                        <tr>
                            <th >Phone</th>
                            <td><input type="text" name="phone" value="<?php echo $req_cliente->__GET('phone'); ?>"  /></td>
                        </tr>
                        <tr>
                            <th >Mensaje</th>
                            <td><input type="text" name="message" value="<?php echo $req_cliente->__GET('message'); ?>"  /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="pure-button pure-button-primary">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th >Name</th>
                            <th >Email</th>
                            <th >Phone</th>
                            <th >Message</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <?php foreach($model->Listar() as $r): ?>
                        <tr>
                            <td><?php echo $r->__GET('name'); ?></td>
                            <td><?php echo $r->__GET('email'); ?></td>
                            <td><?php echo $r->__GET('phone'); ?></td>
                            <td><?php echo $r->__GET('message'); ?></td>
                            <td>
                                <a href="?action=editar&id=<?php echo $r->id; ?>">Editar</a>
                            </td>
                            <td>
                                <a href="?action=eliminar&id=<?php echo $r->id; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>     
              
            </div>
        </div>

    </body>


</html>





